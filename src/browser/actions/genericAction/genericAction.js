
import axios from 'axios';
import {Links} from '../../apiLinks';
const GET_PORTAL_REQUEST="GET_PORTAL_REQUEST";


const getPortalRequest=()=>{
   return dispatch =>{
        return axios.get(Links.getPortals,'',{headers: {'Content-Type':'charset=utf-8'}}).then((res)=>{
            dispatch(fetchPortalReq(res));    
        });
    }
}
const fetchPortalReq=(res)=>{
    return {
        type:GET_PORTAL_REQUEST,
        payload:res.data
    }
}

export  default{
    getPortalRequest,
    fetchPortalReq,
    GET_PORTAL_REQUEST

}