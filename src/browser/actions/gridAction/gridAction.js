
import axios from 'axios';
import {Links} from '../../apiLinks';
const GET_CSV_REQUEST="GET_CSV_REQUEST";


const getGridRequest=()=>{
   return dispatch =>{
        return axios.get(Links.getGrid,'',{headers: {'content-type':'application/x-www-form-urlencoded'}}).then((res)=>{
            dispatch(fetchGridReq(res));    
        });
    }
    
}
const fetchGridReq=(res)=>{
    return {
        type:GET_CSV_REQUEST,
        payload:res
    }
}

export  default{
    getGridRequest,
    fetchGridReq,
    GET_CSV_REQUEST

}