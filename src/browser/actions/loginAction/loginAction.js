
import axios from 'axios';
import {Links} from '../../apiLinks';
const GET_LOGIN_REQUEST="GET_LOGIN_REQUEST";

const GET_LOGOUT_REQUEST="GET_LOGOUT_REQUEST";



const getLoginRequest=(req)=>{
   return dispatch =>{
        return axios.post(Links.getLogin,req).then((res)=>{
            dispatch(fetchLoginReq(res));    
        });
    }
    
}
const fetchLoginReq=(res)=>{
    return {
        type:GET_LOGIN_REQUEST,
        payload:res.data.resData
    }
}

const getLogOutRequest=()=>{
    return {
        type:GET_LOGOUT_REQUEST
    }
     
 }
export  default{
    getLoginRequest,
    GET_LOGIN_REQUEST,
    fetchLoginReq,
    GET_LOGOUT_REQUEST,
    getLogOutRequest

}