
import {gridApiLinks} from './gridApi/gridApi';
import {genericApiLinks} from './genericApi/genericApi';

var prefixHost="";
if(location.href.indexOf('3000')>-1)
{
    prefixHost="";
}
else if(location.href.indexOf('file')>-1)
{
    prefixHost=location.href.split('/index')[0];
}
else
{
    prefixHost=location.href.split('/#')[0];
}

const Links={
    getGrid:prefixHost+gridApiLinks.getGridDetails,
    getPortals:prefixHost+genericApiLinks.getGenericItems,
}
export {
    Links
}