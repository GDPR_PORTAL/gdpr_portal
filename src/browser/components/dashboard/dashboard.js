import React,{Component} from 'react';
import {bindActionCreators} from 'redux';
import { Row, Col, Button } from 'react-bootstrap';
import {connect} from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import genericAction from '../../actions/genericAction/genericAction';
import gridAction from '../../actions/gridAction/gridAction';
import cx from 'classnames';
import { withRouter } from 'react-router-dom';
import PieChart from "react-svg-piechart";
import BarChart from "react-svg-bar-chart"
import  "./dashboard.css";

class dashboard extends Component{
    constructor(props){
        super(props);
        this.postBooler=false;
        this.majorVulArr=[];
        this.majorVulVal='';
        this.state={
            personImage:'',
            activePage: 1,
            personData:[],mulArr2:[]
        }
    }
 
    componentWillMount(){
        if(sessionStorage.validUser=='false' || sessionStorage.validUser==undefined)
        {
            this.props.history.push('/');
        }
        else
        {
             this.props.getPortalRequest();
             this.props.getGridRequest();
             this.greater();
        }
    }
      componentWillReceiveProps(newProps)
      {
        if(newProps.gridDetails.length)
            this.setState({gridDetails:newProps.gridDetails});
      }
      vulnerabilities=(index,flag)=>{
        var inc=0;
        this.state.gridDetails.map((x,y)=>{
            if(this.state.gridDetails[y][index]=="FAIL")
            {
               inc++;
            }
        });
        if(flag)
        this.majorVulArr.push(inc);

        return inc;
      }
      centVulnerabilities=(index,flag)=>{
          var cents=(this.vulnerabilities(index,flag) * 100)/this.state.gridDetails.length;
          return Math.round(cents);
      }
      findGreater=(objArr)=>{
        var objArr=objArr.sort().reverse().filter((x,y)=>{return (x >0)});
        var finder=0;
        for (var i=0; i<=finder;i++){
            if (objArr[i]>finder) {
                 finder=objArr[i];
            }
        }
        var dta=JSON.parse(sessionStorage.mullArr);
        return {greaterVal:finder,finderIndex:dta.indexOf(finder)}
      }
      barChartCalculation=()=>{
          var arr=[];
          for(var a=8;a<this.props.gridDetails[0].length;a++){
            arr.push(this.centVulnerabilities(a,false));
          }
          return arr;
      }
       piechartOne=(chart)=>{
        let data=[];
        if(this.state.gridDetails.length)
        {
           var dta= this.centVulnerabilities(7,false);
        }
        if(chart=="bar")
        {
            var arr=this.barChartCalculation();
            for (let x = 1; x <= 8; x++) {
                data.push({x: x, y: arr[x]})
            }
        }
        else
        {
             data = [
                {title: (100-dta)+"%", value: (100-dta), color: "#008000"},
                {title: dta+"%", value: dta, color: "#FF0000"}
              ]
        }
          return data;
    }

     piechartTwo=()=>{
        var data=[];
        this.majorVulArr=[];
        if(this.state.gridDetails.length)
        {
          var colorsArr=["","","","","","","","","#E32636","#ffdb58","#8F9779","#A52A2A","#568203","#3D2B1F","#3B2F2F","#0093AF"];
           for(var i=8;i< this.state.gridDetails[0].length;i++){
             var dta= this.centVulnerabilities(i,true);
             data.push(
                {title: this.state.gridDetails[0][i], value:dta, color: colorsArr[i]},
             );
            } 
             var arr=[0,0,0,0,0,0,0,0,...this.majorVulArr];
             sessionStorage.mullArr=JSON.stringify(arr);
            this.majorVulVal=this.findGreater(arr);     
        }     
        return data;
    }
    greater=()=>
    {
        var self=this;
        var timer=setInterval(()=>{
            if(this.majorVulVal!="" && this.majorVulVal!=undefined)
            {
             self.setState({majorVul:self.state.gridDetails[0][this.majorVulVal.finderIndex]});
             clearInterval(timer);
            }

        },20)
        
        
    }
    render(){
        const margin = {top: 20, right: 20, bottom: 30, left: 40};
        const {portals }= this.props;
        return (
            <div className="view">
                <div className="personals">
                    <div className="page-heading"> 
                        <img src="src/browser/images/logo-white.png" className="img-responsive loreal-img" alt="loreal"/>
                        <h3 className="heading-name">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.title:''}</h3>
                    </div>
                    <div className="personal_container" >
                                <div className="dashboard">
                                    <div className="container-fluid dashboard-container">
                                        <Col lg={12} sm={12} xs={12} className="dash-dropdown hide">
                                            <Col  lg={3} sm={3} xs={12} className="ddl">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="opel">Opel</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </Col>
                                            <Col lg={3} sm={3} xs={12} className="ddl ">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="opel">Opel</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </Col>
                                            <Col lg={3} sm={3} xs={12} className="ddl ">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="opel">Opel</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </Col>
                                        </Col>
                                        <Col lg={12} sm={12} xs={12} className="callout-flex">
                                            <Col lg={3} md={3} sm={3} xs={12} className="calloutContainer">
                                            <div className="callout">
                                                <p className="top-left">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.totalCollectionText:''}</p>
                                                <p className="numbers">{(this.state.gridDetails)?this.state.gridDetails.length :''}</p>
                                               </div>
                                            </Col>
                                            <Col lg={3} md={3}  sm={3} xs={12} className="calloutContainer">
                                            <div className="callout">
                                                <p className="top-left">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.invalidCollectionText:''}</p>
                                                <p className="numbers">{(this.state.gridDetails)?this.vulnerabilities(7,false):''}</p>
                                               </div>
                                            </Col> 
                                            <Col lg={3} sm={3} md={3} xs={12} className="calloutContainer">
                                            <div className="callout">
                                                <p className="top-left">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.invalidRecords:''}</p>  
                                                <p className="numbers">{(this.state.gridDetails)?this.centVulnerabilities(7,false)+'%':''}</p>   
                                            </div>
                                            </Col>
                                            <Col lg={3} sm={3} md={3} xs={12} className="calloutContainer">
                                            <div className="callout">
                                                <p className="top-left">Maximum failures in validation checks:</p>  
                                                <p className="numbers">{(this.state.majorVul)?this.state.majorVul:''}</p>   
                                                </div>
                                            </Col>     
                                       </Col>
                                       <Col lg={12} sm={12} md={12} xs={12}>
                                       <div className="calloutContent">
                                            <Col lg={4} sm={12} md={12} xs={12}  className="callout-2">
                                                <p className="numbers-2">                                              
                                                    <PieChart
                                                            data={(this.state.gridDetails)?this.piechartOne():[]}
                                                            expandOnHover
                                                    />
                                                </p>
                                                <Col lg={12} sm={12} md={12} xs={12}>
                                                    <Col lg={6} sm={6} md={6} xs={6} className="color-callout-1 color-first">
                                                        <div className="boxContent">
                                                            <span className="color-span black-bg"></span><p className="color-desc">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.validColText:''}</p>
                                                        </div>
                                                    </Col>
                                                    <Col lg={6} sm={6} md={6} xs={6} className="color-callout-2 color-first">
                                                        <div className="boxContent">
                                                            <span className="color-span blue-bg"></span><p className="color-desc-2">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.invalidColText:''}</p>
                                                        </div>
                                                    </Col>
                                                </Col>
                                            </Col>
                                            <Col lg={4} sm={12} md={12} xs={12}  className="callout-2 pieChart">
                                                <p className="numbers-2">
                                                   <PieChart
                                                            data={(this.state.gridDetails)?this.piechartTwo(null):[]}
                                                            expandOnHover
                                                    />
                                                </p>
                                                <Col lg={12} sm={12} md={12} xs={12}>
                                                    <Col lg={6} sm={6} md={6} xs={12} className="color-callout-1">

                                                        <div className="boxContent">
                                                            <span className="color-span red-bg"></span><span className="red-txt">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.calloutOneText[0]:''}</span>
                                                        </div>
                                                        <div className="boxContent">
                                                            <span className="color-span dark-green-bg"></span><span className="dark-green-txt">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.calloutOneText[1]:''}</span>
                                                        </div>
                                                        <div className="boxContent">
                                                            <span className="color-span light-green-bg"></span><span className="light-green-txt">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.calloutOneText[2]:''}</span>
                                                        </div>
                                                        <div className="boxContent">
                                                            <span className="color-span green-bg"></span><span className="green-txt">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.calloutOneText[3]:''}</span>
                                                        </div>
                                                        <div className="boxContent">
                                                            <span className="color-span maroon-bg"></span><span className="maroon-txt">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.calloutOneText[4]:''}</span>
                                                        </div>
                                                    </Col>
                                                    <Col lg={6} sm={6} md={6} xs={6} className="color-callout-2">
                                                        <div className="boxContent">
                                                            <span className="color-span light-brown-bg"></span><span className="light-brown-txt">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.calloutTwoText[0]:''}</span>
                                                        </div>
                                                        <div className="boxContent">
                                                            <span className="color-span light-blue-bg"></span><span className="light-blue-txt">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.calloutTwoText[1]:''}</span>
                                                        </div>
                                                        <div className="boxContent">
                                                            <span className="color-span orange-bg"></span><span className="orange-txt">{(portals.hasOwnProperty('dashboard'))?portals.dashboard.calloutTwoText[2]:''}</span>
                                                        </div>
                                                    </Col>
                                                </Col>
                                            </Col>

                                            <Col lg={4} sm={12} md={12} xs={12} className="callout-2">
                                                <p className="numbers barChart">
                                                    <BarChart 
                                                        data={(this.state.gridDetails)?this.piechartOne('bar'):[]}
                                                    />   
                                                </p>  
                                             </Col>
                                             </div>
                                        </Col> 
                                    </div>
                                </div>
 
                    </div>
                </div>
            </div>
         
            );
    }
};
const mapStateToProps=(state,y)=>{
    return{
        portals:state.getPortals,
        gridDetails:state.gridDetails || []

    }

}
const matchDispatchProps=(dis)=>{

    return bindActionCreators(Object.assign(gridAction,genericAction),dis);
}

export default withRouter(connect(mapStateToProps,matchDispatchProps)(dashboard));