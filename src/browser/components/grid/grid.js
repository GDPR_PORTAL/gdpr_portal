import React,{Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { withRouter } from 'react-router-dom';
import  "./grid.css";
import cx from 'classnames';
import gridAction from '../../actions/gridAction/gridAction';
import Pagination from "react-js-pagination";

class grid extends Component{
    constructor(props){
        super(props);
        this.state={
            activePage: 1,
            personTotal:0,
            personData:[],
            getMenuIndex:"",
            filterData:[]
        }
        
    }  
    hoverOn=(event)=>{
        this.hovers(event,true);
      }
      hoverOff=(event)=>{ 
        this.hovers(event,false);   
      }
      hovers=(event,boolObj)=>
      {
        let indexer=event.target.parentElement.rowIndex-1;
        let indexObj= 'row_hover_'+indexer;
        let obj={};
        obj[indexObj]=boolObj;
        this.setState(obj);
      }
      hoverPopOff=()=>{
        var elems = document.querySelectorAll(".popup.hide");

        [].forEach.call(elems, function(el) {
            el.classList.remove("hide");
        });
      }
renderReport=()=>{
  if(this.props.gridDetails.length)
  {
      return  this.state.personData.map((x,y)=>{
        return (
          <tr key={y} className={cx((x[7]=="FAIL")?"colorRed":"colorGreen")} onMouseEnter={this.hoverOn} 
          onMouseLeave={this.hoverOff}>
              { x.map((a,b)=>{
                return(
                  <React.Fragment>
                         <td  key={b}>{(a.replace(/�/g, 'e') )}
                         {(b==3)?<div pops={`row_hover_${y}`} className={cx("popup",this.state['row_hover_'+y] ? "hide" : "")}    onMouseLeave={this.hoverPopOff}>
                                        <div className="modal-header">
                                            <h4 className="heading4">Collection Point Details - PRD</h4>
                                        </div>
                                        <div className="modal-body">
                                            {this.state.personHeadData.map((c,d)=>{
                                                    return(
                                                        <React.Fragment>
                                                            <div  key={d}>
                                                                <p className={cx("heading",(d<5)?'caption-bolder':'')}>{c}</p><span className="colon-span">:</span>
                                                                <p className={cx("value",(d<5)?'caption-bolder':'')}>{x[d].replace(/�/g, 'e')}</p>
                                                            </div>
                                                        </React.Fragment>
                                                    );
                                                })}
                                        </div>              
                                 </div>:''}
                         </td>
                  </React.Fragment>
                ); 
              })}
               
             </tr>
        )
      }
      );
  }
}
componentWillMount()
{
    if(sessionStorage.validUser=='false' || sessionStorage.validUser==undefined)
    {
        this.props.history.push('/');
    }
    this.props.getGridRequest();
}
componentWillReceiveProps(newProps)
{
    if(Object.keys(newProps.gridDetails).length !== 0 && newProps.gridDetails.constructor !== Object)
    {
      var dta=newProps.gridDetails.filter((x,y)=>{return (y>=1 && y<21)});
      this.setState({personData:dta,personHeadData:newProps.gridDetails[0],personTotal:newProps.gridDetails.length});
    }
}
handlePageChange=(pageNumber)=>{
    if(this.state.getMenuIndex>0){
        var dta=this.filterData.filter((x,y)=>{return (y>=(pageNumber-1)*21)&& (y>0) && (y<(pageNumber*21))});
        this.setState({activePage: pageNumber,personData:dta,personHeadData:this.props.gridDetails[0],personTotal:this.filterData.length});      
    }
    else{
         var dta=this.props.gridDetails.filter((x,y)=>{return (y>=(pageNumber-1)*21)&& (y>0) && (y<(pageNumber*21))});
         this.setState({activePage: pageNumber,personData:dta,personHeadData:this.props.gridDetails[0],personTotal:this.props.gridDetails.length});
    }
   this.getSubmenu();
  }
  getFilter=(event)=>{
      this.setState({getMenuIndex:event.target.selectedOptions[0].value});
  }
  getSubmenu=()=>
  {
    var arr=[];
    if(Object.keys(this.props.gridDetails).length !== 0 && this.props.gridDetails.constructor !== Object)
    {
                    this.props.gridDetails.map((x,y)=>{
                        if(y>0)
                        arr.push(x[this.state.getMenuIndex]);
                    });
                var data=this.removeDuplicates(arr).sort();
                return data.filter((x)=>{return x!=undefined}).map((x,y)=>{
                        return(
                        <option key={y} value={x}>{(x!=undefined)?x.replace(/�/g, 'e'):null}</option>
                        )
                    });
     
    }
  }
 removeDuplicates=(arr)=>{
    let unique_array = arr.filter((elem, index, self) =>{
        return index == self.indexOf(elem);
    });
    return unique_array;
  }
  subFilter=()=>{
    var self=this,dta=[];
    if(Object.keys(this.props.gridDetails).length !== 0 && this.props.gridDetails.constructor !== Object)
    {
       if(self.filterVal.options.length>0){
        self.props.gridDetails.map((a,b)=>{
                a.map((x,y)=>{
                    if(x.toLowerCase().trim()==self.filterVal.selectedOptions[0].value.toLowerCase().trim())
                     dta.push(a);
                });
        });
        this.filterData=dta;  
               this.setState({activePage: 1,personData:dta.filter((x,y)=>{return (y>=0 && y<20)}),personHeadData:this.props.gridDetails[0],personTotal:dta.length});
        }   
    }
}
render(){ 
      return (
        <React.Fragment>
        <div className="view">
           <div className="time-container">
                <div className="page-heading"> 
                   <h3 className="heading-name">Collection Point - PRD</h3>
                   <img src="src/browser/images/logo-white.png" className="img-responsive loreal-grid-img" alt="loreal"/>
                </div>
              </div>
              <div className="grid-report-container">
              <Col lg={12} sm={12} md={12} xs={12} className="filter-section">
                    <Col lg={5} sm={12} md={5} xs={12} className="filter-ddl">
                        <select onChange={this.getFilter}>
                            <option value="">------None------</option>
                            {(this.state.personHeadData)?this.state.personHeadData.map((c,d)=>{
                                                    return(
                                                        <React.Fragment>
                                                           
                                                                <option key={d} value={d}>{c}</option>
                                                            
                                                        </React.Fragment>
                                                    );
                                                }):null}
                        </select>
                    </Col> 
                    <Col lg={5} sm={12} md={5} xs={12} className="filter-ddl">
                        <select ref={(el)=>this.filterVal=el}>
                           {(this.state.getMenuIndex!="")?this.getSubmenu():null}
                           
                        </select>
                    </Col> 
                    <Col lg={2} sm={12} md={2} xs={12} className="submit-btn">
                        <button type="button" onClick={this.subFilter}>Submit</button>
                    </Col>
              </Col>
                <table className="table table-bordered result-table">
                    <thead>
                        <tr>
                        {this.state.personHeadData?this.state.personHeadData.map((x,y)=>{
                            return( <th key={y}>{x}</th>);
                        }):null}
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderReport()}
                    </tbody>
                </table>
                <div className="pagination_container">
                         {this.state.personTotal>0?<Pagination
                                activePage={this.state.activePage}
                                itemsCountPerPage={21}
                                totalItemsCount={this.state.personTotal}
                                pageRangeDisplayed={21}
                                onChange={this.handlePageChange}
                           />:null}
                    </div>
              </div>
        </div>
        
        </React.Fragment>
      );
    }
};
const mapStateToProps=(state,y)=>{
    return{
      gridDetails:state.gridDetails || []

    }

}
const matchDispatchProps=(dis)=>{

    return bindActionCreators(gridAction,dis);
}

export default withRouter(connect(mapStateToProps,matchDispatchProps)(grid));