import React,{Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import loginCss from  "./login.css";
import genericAction from '../../actions/genericAction/genericAction';
class login extends Component{
    constructor(props){
        super(props);
        this.state={
            nameErrorText:''
        }
    }
    componentWillMount()
    {
        this.props.getPortalRequest();
    }
    LoginFetch=()=>{
        const {user,password,errorText}=this.props.getPortals.login.credentials;
        if (this.username.value==user.name && this.password.value == password.pswd)
        {
            this.setState({ErrorText:''});
            sessionStorage.validUser=true;
            this.props.history.push('/dashboard');
        }
        else
        {
            this.setState({ErrorText:errorText});
        }
      

    }
    render(){
        const {getPortals}=this.props;
        return(
            <React.Fragment>
                <div className="row login-page">
                    <div className="main-container">
                        <img src="src/browser/images/loreal.png" className="img-responsive loreal-img" alt="loreal"/>
                        <img src="src/browser/images/onetrust.png" className="img-responsive one-trust-img" alt="onetrust"/>
                    </div>
                </div>
                <section className="gdpr">
                    <h1 className="head1">{(getPortals.hasOwnProperty('login'))?getPortals.login.title:''}</h1>
                    <div className="login">
                        <div className="main-container">
                                <div className="container form-container">
                                    <label  className="label-form">{(getPortals.hasOwnProperty('login'))?getPortals.login.name.caption:''}</label>
                                    <input type="text" className="input-type" ref={(el)=>{this.username=el;}} placeholder={(getPortals.hasOwnProperty('login'))?getPortals.login.name.placeholder:''} name="uname" required />
                                    <label  className="label-form">{(getPortals.hasOwnProperty('login'))?getPortals.login.password.caption:''}</label>
                                    <input type="password" className="input-type" ref={(el)=>{this.password=el;}} placeholder={(getPortals.hasOwnProperty('login'))?getPortals.login.password.placeholder:''} name="psw" required />
                                    <span className="error-text">{this.state.ErrorText || ''}</span>
                                    <button type="button" className="submit-button" onClick={()=>{this.LoginFetch()}}>
                                       Login
                                    </button>
                                </div>
                        </div>
                    </div>
                </section>
        </React.Fragment>
        );
    }
};
const mapStateToProps=(state,y)=>{
    return{
        loginStatus:state.loginStatus,
        getPortals:state.getPortals

    }

}
const matchDispatchProps=(dis)=>{

    return bindActionCreators(genericAction,dis);
}

export default withRouter(connect(mapStateToProps,matchDispatchProps)(login));