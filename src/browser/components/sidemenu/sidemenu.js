import React,{Component} from 'react';
import {Router,Link,withRouter} from 'react-router-dom';
import cx from 'classnames';
import   "./sidemenu.css";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class Sidemenu extends Component{
    constructor(props){
      super(props);
      this.obj={menuItems:[]};
      this.hamburgerButtonClik=this.hamburgerButtonClik.bind(this);
      this.logout=this.logout.bind(this);
      this.flag=true;
      this.state={
        HHB_status:false
      }
      this.menuSelector=this.menuSelector.bind(this);
    }
    menuSelector(event)
    {
      var index =Array.prototype.indexOf.call(event.currentTarget.parentElement.childNodes,event.currentTarget);
      let obj={};
      event.currentTarget.parentElement.childNodes.forEach((x,y)=>{
        (y==index)?obj['menu_select_'+y]=true:obj['menu_select_'+y]=false;
      });
      this.setState(obj);
    
    }
    loadSelector(match)
    {
      var menuList=['/dashboard','/grid'];
      var indx=menuList.indexOf(match);
      if(indx>-1)
      {
        let obj={};
        menuList.forEach((x,y)=>{
          (y==indx)?obj['menu_select_'+y]=true:obj['menu_select_'+y]=false;
        });
        this.setState(obj); 
      }

    }
    componentWillMount()
    {
      this.loadSelector(this.props.history.location.pathname);
    }
    componentWillReceiveProps(newProps)
    {
      if(newProps.loginStatus==false)
      {
        this.props.history.push("");
      }

    }
    hamburgerButtonClik()
    {
      if(this.flag){
        this.props.HBBTrack(this.flag);
        this.setState({HHB_status:this.flag});
        this.flag=false;
      }
      else
      {
        this.props.HBBTrack(this.flag);
        this.setState({HHB_status:this.flag});
        this.flag=true;
      }
    }
    logout(){
      sessionStorage.validUser=false;
      this.props.history.push("/");
    }
  componentWillReceiveProps(newProps)
  {
    this.obj=newProps.dta;
  }
 render(){
    const lists=(Object.keys(this.obj.menuItems).length === 0 && this.obj.menuItems.constructor === Object)?null: this.obj.menuItems.map((a,b) =>(
        <li key={a.id} onClick={this.menuSelector} className={cx(this.state['menu_select_'+b]?'active':'')}>
             {
               (a.link)?<Link to={a.link}>{a.menuName} <img className="img-responsive link-icon" src={require(`../../images/icons/${a.icon}.png`)}></img></Link>
              :
              <a onClick={this.logout}>{a.menuName} <img className="img-responsive link-icon" src={require(`../../images/icons/${a.icon}.png`)}></img></a>
              }
        </li>
      ));
      const ActiveStatus=(this.state.HHB_status)?'active':{};
      return (
        <div className={cx("SideMenu",ActiveStatus)} >
          <div className="header-wrap">
            <label className="sideMenuCaption">{this.obj.title}</label>
            <div id="ham-btn" onClick={this.hamburgerButtonClik} className={cx(ActiveStatus)}>  
              <div id='top'></div> 
              <div id='middle'></div>
              <div id='bottom'></div>
            </div>
          </div>
         
              <ul className="menuList">
                {lists}
              </ul>
      
      </div>
      );
}

};

const mapStateToProps=(state,y)=>{
    return{
      loginStatus:state.loginStatus

    }

}
const matchDispatchProps=(dis)=>{

    return bindActionCreators({},dis);
}

export default withRouter(connect(mapStateToProps,matchDispatchProps)(Sidemenu));
