
import React from 'react';
import ReactDOM from  'react-dom';
//import Report from  './components/report';
import {createStore,applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
//import createSagaMiddleware from 'redux-saga';
import {logger} from 'redux-logger';
//import stonerockSagas  from './sagas';
import allReducers from './reducers';
import Roots from  './views/indexView'; /* all views  */
import {HashRouter as Router,Route,Switch,Link} from 'react-router-dom';
import createBrowserHistory from 'history/createHashHistory';
const Browserhistory = createBrowserHistory();
import {syncHistoryWithStore } from 'react-router-redux';

//const sagaMiddleware = createSagaMiddleware();sagaMiddleware,
const store= createStore(allReducers,applyMiddleware(thunk,logger));
const history = syncHistoryWithStore(Browserhistory, store);
//sagaMiddleware.run(stonerockSagas);
//<Route  path="*"  component={NoMatch}  />  

const NoMatch = () =>{ return(<div className="text-center">
  <h3 className="text-danger">404: Page not found!</h3>
  <Link to="/">Back To Login</Link>
</div>)};


ReactDOM.render(
        <Provider store={store}>
                <Router history={history} >
                    <Switch>  
                    
                        <Route exact  path="/" render={(props)=><Roots.loginView {...props}  />}  />
                       
                                <Roots.rootView > 
                                        <Route exact path="/dashboard" render={(props)=><Roots.dashboardView {...props}  />}  />
                                        <Route exact path="/grid"  render={(props)=><Roots.gridView {...props}  />}   />
                                       
                                </Roots.rootView>    
                                
                          
                        </Switch>     
                </Router> 
               
        </Provider>,
document.getElementById('root')
);


