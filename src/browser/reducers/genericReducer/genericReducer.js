
const genericReducer=(state = {}, action)=> {
  switch(action.type)
  {
    case "GET_PORTAL_REQUEST":{
      return action.payload;
    }
    default:
    return state;
  }
  
}
 
export  {genericReducer};
