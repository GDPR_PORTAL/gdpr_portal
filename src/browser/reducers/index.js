import {combineReducers} from 'redux';
import {gridReducer} from '../reducers/gridReducer/gridReducer';
import {genericReducer} from '../reducers/genericReducer/genericReducer';
import {loginReducer} from '../reducers/loginReducer/loginReducer';

import { routerReducer } from 'react-router-redux';
const allReducers=combineReducers(
  {
  // form:formReducer,
   routing: routerReducer,
   gridDetails:gridReducer,
   getPortals:genericReducer,
   loginStatus:loginReducer
  }
);
export default allReducers;
