
const loginReducer=(state = {}, action)=> {
  switch(action.type)
  {
    case "GET_LOGIN_REQUEST":{
      return action.payload;
    }
    case "GET_LOGOUT_REQUEST":{
      return false;
    } 
    default:
    return state;
  }
  
}
 
export  {loginReducer};
