import React, { Component } from 'react';
import Dashboard from '../../components/dashboard/dashboard';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';

class personalView extends Component
{
    constructor(props){
        super(props);
    }

    render()
    {
        return(
            <Dashboard />
        );
    }

}
const mapStateToProps=(state,y)=>{
    return{
        form:state.form,
        data:state.data

    }

}
const matchDispatchProps=(dis)=>{

    return bindActionCreators({},dis);
}

export default withRouter(connect(mapStateToProps,matchDispatchProps)(personalView));
