import React, { Component } from 'react';
import Grid from '../../components/grid/grid';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
//import {requester,postRequestData} from '../../actions';
import { withRouter } from 'react-router-dom';

class gridView extends Component
{
    constructor(props){
        super(props);
    }

    render()
    {
        return(
            <Grid />
        );
    }

}
const mapStateToProps=(state,y)=>{
    return{
        form:state.form,
        data:state.data

    }

}
const matchDispatchProps=(dis)=>{
    return bindActionCreators({},dis);
}

export default withRouter(connect(mapStateToProps,matchDispatchProps)(gridView));
