import rootView from  '../views/rootView';
import loginView from  '../views/loginView/loginView';
import dashboardView from  '../views/dashboardView/dashboardView';
import gridView from  '../views/gridView/gridView';

export default {
    rootView,loginView,dashboardView,gridView
}
