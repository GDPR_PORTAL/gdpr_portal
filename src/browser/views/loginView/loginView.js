import React, { Component } from 'react';
import Login from '../../components/login/login';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
//import {requester,postRequestData} from '../../actions';
import { withRouter } from 'react-router-dom';
class loginView extends Component
{
    constructor(props){
        super(props);
    }

    render()
    {
        return(
            <Login />
        );
    }

}
const mapStateToProps=(state,y)=>{
    return{
        form:state.form,
        data:state.data

    }

}
const matchDispatchProps=(dis)=>{

    return bindActionCreators({},dis);
}

export default withRouter(connect(mapStateToProps,matchDispatchProps)(loginView));
