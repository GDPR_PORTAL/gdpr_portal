import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import genericAction from '../actions/genericAction/genericAction';
import { withRouter } from 'react-router-dom';
import Sidemenu from '../components/sidemenu/sidemenu';
import cx from 'classnames';

class RootView extends Component{
    constructor(props){
        super(props);
        this.state={
            ham_open:false
        }
      this.HamburgerBtnTrack=this.HamburgerBtnTrack.bind(this);
    }
    HamburgerBtnTrack(obj)
    {
        this.setState({ham_open:obj});
    }
    componentWillMount()
    {
        this.props.getPortalRequest();
    }
render(){
    const { props } = this;
    const { children,getPortals} = props;

    
    return(
        <React.Fragment>
            <Sidemenu dta={getPortals.sidemenu || []} HBBTrack={this.HamburgerBtnTrack} /> 
            <div className={cx("content-wrapper-section",(this.state.ham_open)?'active':{})}>
            {children}
            </div>
        </React.Fragment>
    );
 }
}
const mapStateToProps=(state,y)=>{
    return{
        getPortals:state.getPortals

    }

}
const matchDispatchProps=(dis)=>{

    return bindActionCreators(genericAction,dis);
}

export default withRouter(connect(mapStateToProps,matchDispatchProps)(RootView));