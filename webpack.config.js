const path= require('path');
const htmlWebpackPlugin=require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractCssChunks=require('extract-css-chunks-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
require("babel-core/register");
require("babel-polyfill");
let pathsToClean = [
    'dist',
   // 'build'
  ]
module.exports={
    entry:['babel-polyfill', './src/browser/index.js'],
    output:{
        path:path.join(__dirname,'/dist'),
        filename:'js/portal_bundle.js'
    },
    module:{
        rules:[
            {
                test:/\.js$/,
                exclude:/node_modules/,
                use:{
                    loader:'babel-loader'
                }
            },
            {
                test:/\.css$/,
                use:[
                    ExtractCssChunks.loader,
                    "css-loader",
                    "classnames"
                ]
            },
            {
              test: /\.(otf|eot|png|jpe?g|svg|ttf|woff|woff2|gif)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
              use:["file-loader?name=[path][name].[ext]"],
             
              exclude: /node_modules/
            }
        ]
    },
    devServer: {
       // devtool: 'eval',
        port: 3000,
        open: true,
        hot: true,
        inline: true,
        historyApiFallback: true,
       /* proxy: {
          "/": "http://localhost:8080"
        }*/
      },
    plugins:[
        new htmlWebpackPlugin({
            template:'./src/browser/index.html'
        }),
        new CleanWebpackPlugin(pathsToClean),
        new ExtractCssChunks(
            {
              filename: "./css/portal.css",
              chunkFilename: "[id].css",
              hot: true
            }
        ),
        new CopyWebpackPlugin([{
            from: 'src/browser/index.html'
        }, {
            from: 'src/browser/images',
            to: 'src/browser/images'
        },{
            from: 'src/browser/mocks',
            to: 'src/browser/mocks'
        },,{
            from: 'src/browser/settings.json',
            to: 'src/browser/settings.json'
        }])
    ]
}